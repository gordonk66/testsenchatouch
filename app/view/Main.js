Ext.define('TestApp.view.Main', {
    extend: 'Ext.Panel',
    xtype: 'main',
    requires: [
    ],
    config: {

        items: [
            {
                title: 'Welcome',
                iconCls: 'home',

                styleHtmlContent: true,
                scrollable: true,

                items: {
                    docked: 'top',
                    xtype: 'titlebar',
                    title: 'Welcome to Sencha Touch 2'
                },

                html: [
                    "Initial page"
                ].join("")
            },
            {
                title: 'Get Started',
                iconCls: 'action',

                items: [
                    {
                        docked: 'top',
                        xtype: 'titlebar',
                        title: 'Getting Started'
                    }
                ],
	            html: [
		            "Page1 page"
	            ].join("")
            }
        ]
    }
});
